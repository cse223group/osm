package osm

type TileKey struct {
	Point Point
	Zoom  int64
	X     int
	Y     int
}

func NewTileKey(point Point, zoom int64) TileKey {
	return TileKey{
		Point: point,
		Zoom:  zoom,
	}
}
