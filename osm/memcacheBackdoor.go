package osm

import (
	"fmt"
	"github.com/bradfitz/gomemcache/memcache"
	// "log"
)

type Backdoor struct {
	memcacheClient *memcache.Client
}

const (
	serverAddress    = "localhost:11211"
	queryKeyTemplate = "/hot/%d/%d/%d.png"
)

var singleBackdoor *Backdoor

func CreateBackdoor() *Backdoor {
	if singleBackdoor == nil {
		singleBackdoor = &Backdoor{memcacheClient: memcache.New(serverAddress)}
		// log.Println("instantiating memcache backdoor")
	}
	return singleBackdoor
}

// Converts TileKey to query string
func TileKeyToQueryKey(tileKey TileKey) string {
	x, y, z := TileKeyToXyz(tileKey)
	return fmt.Sprintf(queryKeyTemplate, x, y, z)
}

// Gets the binary value for the tile stored for TileKey
func (b *Backdoor) GetTileFromCache(tileKey TileKey) ([]byte, error) {
	item, err := b.memcacheClient.Get(TileKeyToQueryKey(tileKey))
	if err == nil {
		return item.Value, nil
	} else {
		return nil, err
	}
}

// Checks if tile for TileKey exists
func (b *Backdoor) ExistsOnShim(tileKey TileKey) (bool, error) {
	return b.KeyExists(TileKeyToQueryKey(tileKey))
}

func (b *Backdoor) onOsmCache(tileKey TileKey) bool {
	x, y, z := TileKeyToXyz(tileKey)

	//unset last 3 bits for x and y
	xp := x & ^7
	yp := y & ^7

	key := fmt.Sprintf("ajt/%d/%d/%d.meta", xp, yp, z)
	exits, _ := b.KeyExists(key)
	return exits
}

func (b *Backdoor) KeyExists(key string) (bool, error) {
	_, err := b.memcacheClient.Get(key)
	if err == nil {
		return true, nil
	} else if err == memcache.ErrCacheMiss {
		return false, nil
	} else {
		return false, err
	}
}

// Sets the binary data for TileKey
func (b *Backdoor) SetTileInCache(tileKey TileKey, value []byte) error {
	item := &memcache.Item{Key: TileKeyToQueryKey(tileKey), Value: []byte(value)}
	return b.memcacheClient.Set(item)
}
