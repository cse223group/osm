package osm

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

/*
	Informs 2-level ancestor about dead parent.
	For global failure mitigation, it orphans its children and dies.
	For local failure mitigation, it does nothing since the ancestor has to message now.

*/
func (node *NodeState) HandleParentDeath() {

	anc := node.Ancestors[len(node.Ancestors)-1]

	urlString := anc + "/deadChild"

	resp, err := http.PostForm(urlString,
		url.Values{"dead": {node.Parent}, "myLoad": {strconv.FormatUint(node.MyLoad, 64)}})

	if err != nil || Config.USE_LOCAL_ORPHAN_MITIGATION == false {
		node.OrphanKidsAndDie()
	}

	if resp.StatusCode != 200 {
		node.OrphanKidsAndDie()
	}

	defer resp.Body.Close()
}

/*
	Removes children from the tree and orphans itself for global mitigation
*/
func (node *NodeState) OrphanKidsAndDie() {
	if node.Children != nil {
		for _, child := range node.Children {
			resp, err := http.Get("http://" + child + ":8000" + "/orphaned")
			if err != nil {
				log.Println(err.Error())
				return
			}
			defer resp.Body.Close()
		}
	}
	node.MySubtreeLoad = 0
	node.HandleOrphaned()
}

/*
	For global orphan mitigation, invalidates its own state and tells world node it is free now

*/
func (node *NodeState) HandleOrphaned() {

	world := node.Ancestors[0]

	for {
		b, _ := json.Marshal(NodeState{})
		resp, _ := http.Post("http://"+world+":8000"+"/freeNode", "application/json", bytes.NewReader(b))
		respContent, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err.Error())
			return
		}
		var state AddState
		json.Unmarshal(respContent, &state)
		defer resp.Body.Close()
		if state.Done == true {
			node.Parent = state.Node.Parent
			node.Ancestors = state.Node.Ancestors
			node.Region = state.Node.Region
			node.MyLoad = 0
			node.MySubtreeLoad = 0
			node.InformParent()
			break
		} else {
			fmt.Println(state.Node.Ancestors)
			world = state.Node.Ancestors[len(state.Node.Ancestors)-1]
		}
	}

}

/*
	Accepts messages from level-2 descendants, logs information for local mitigation strategy

*/
func (node *NodeState) RegisterChildDeath(clientHost, dead string, load uint64) {

	for k, host := range node.Children {
		if host == dead {
			node.Children[k] = ""
			node.DeathStateInfo.DeadManKey = k
		}
	}

	if Config.USE_LOCAL_ORPHAN_MITIGATION == true {

		node.DeathStateInfo.LoadInfo[clientHost] = load
	}

}

/*
	Waits for 500ms to receive all messages from level-2 descendants, tells the one with least node to be parent
	Informs the other descendants about their new parent
	Local Mitigation strategy

*/
func (node *NodeState) HandleChildDeath() {

	timer := time.NewTimer(time.Millisecond * 500)
	<-timer.C
	var min_load uint64
	min_load = 0
	new_parent := ""
	for k, load := range node.DeathStateInfo.LoadInfo {
		if min_load == 0 {
			min_load = load
		}
		if load <= min_load {
			new_parent = k
		}
	}

	resp1, _ := http.Get(new_parent + "/upgrade?rKey=" + node.DeathStateInfo.DeadManKey)

	defer resp1.Body.Close()

	for k, _ := range node.DeathStateInfo.LoadInfo {
		if k != new_parent {
			go func() {
				resp, _ := http.PostForm(k+"/parentUpdate",
					url.Values{"newParent": {new_parent}})
				resp.Body.Close()
			}()
		}
	}

	node.DeathStateInfo.Valid = false
	for k, _ := range node.DeathStateInfo.LoadInfo {
		delete(node.DeathStateInfo.LoadInfo, k)
	}
}

/*
	Designates new parent, since it has to take over for its parent. Happens only in local mitigation mode

*/
func (node *NodeState) GatherStateInfoForUpgrade() {

	for _, host := range node.Children {
		r, e := http.Get(host + "/getLoad")
		if e == nil {
			responseText, err2 := ioutil.ReadAll(r.Body)
			if err2 != nil {
				log.Printf("Can't read response of ping parent")
			}
			responseStr := string(responseText)
			l, err3 := strconv.ParseUint(responseStr, 10, 64)
			if err3 == nil {
				node.DeathStateInfo.LoadInfo["host"] = l
			}
		}

	}

}
