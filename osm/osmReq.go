package osm

import (
	"log"
	"net/http"
)

func OsmCall(tileKey TileKey, w http.ResponseWriter, hostname string) ([]byte, error, int) {
	memCacheBackdoor := CreateBackdoor()
	var url string
	url = createLookupUrl(osm, osmPort, tileKey)
	onOsmCache := memCacheBackdoor.onOsmCache(tileKey)
	if onOsmCache {
		log.Println("** osm_cache_hit: ", url)
	} else {
		log.Println("** osm_render: ", url)
	}

	tile, err := getTile(url, w)

	if err != nil {
		log.Println(err.Error())
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		return []byte{}, err, http.StatusInternalServerError
	}
	return tile, nil, 200

}
