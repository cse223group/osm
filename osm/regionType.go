package osm

import (
	"errors"
	"math"
)

type Point struct {
	Lat  float64 `json:lat`
	Long float64 `json:long`
}

type RegionBoundary struct {
	UpperLeft  Point `json:upperLeft`
	LowerRight Point `json:lowerRight`
}

func NewPoint(lat, long float64) Point {
	return Point{
		Lat:  lat,
		Long: long,
	}
}

/*
	Checks whether Point p is in the region_boundary
*/
func (r RegionBoundary) isIn(p Point) bool {

	if p.Lat > r.UpperLeft.Lat || p.Lat < r.LowerRight.Lat {
		return false
	}
	if r.UpperLeft.Long > r.LowerRight.Long {
		if p.Long < r.UpperLeft.Long && p.Long > r.LowerRight.Long {
			return false
		}
	}
	if r.UpperLeft.Long < r.LowerRight.Long {
		if p.Long < r.UpperLeft.Long || p.Long > r.LowerRight.Long {
			return false
		}
	}
	return true
}

/*
	For a key "ne", "nw", "se" or "sw", returns the corresponding subregion boundary
	Returns error if the key is wrong
*/
func (r RegionBoundary) GetSubregion(key string) (RegionBoundary, error) {

	var midLat, midLong float64
	midLat = r.UpperLeft.Lat - math.Abs(r.UpperLeft.Lat-r.LowerRight.Lat)/2
	if r.UpperLeft.Long < r.LowerRight.Long {
		midLong = r.UpperLeft.Long + math.Abs(r.UpperLeft.Long-r.LowerRight.Long)/2
	} else {
		if r.UpperLeft.Long+r.LowerRight.Long == 0 {
			midLong = 180.00
		} else if math.Abs(r.UpperLeft.Long) > math.Abs(r.LowerRight.Long) {
			diff := 180 - r.UpperLeft.Long
			apLeft := -180 - diff
			midLong = apLeft + math.Abs(apLeft-r.LowerRight.Long)/2
		} else {
			apRight := 180 + 180 + r.LowerRight.Long
			midLong = r.UpperLeft.Long + math.Abs(r.UpperLeft.Long-apRight)/2
		}
	}

	switch key {
	case "nw":
		lR := Point{Lat: midLat, Long: midLong}
		return RegionBoundary{UpperLeft: r.UpperLeft, LowerRight: lR}, nil
	case "se":
		uL := Point{Lat: midLat, Long: midLong}
		return RegionBoundary{UpperLeft: uL, LowerRight: r.LowerRight}, nil
	case "sw":
		uL := Point{Lat: midLat, Long: r.UpperLeft.Long}
		lR := Point{Lat: r.LowerRight.Lat, Long: midLong}
		return RegionBoundary{UpperLeft: uL, LowerRight: lR}, nil
	case "ne":
		uL := Point{Lat: r.UpperLeft.Lat, Long: midLong}
		lR := Point{Lat: midLat, Long: r.LowerRight.Long}
		return RegionBoundary{UpperLeft: uL, LowerRight: lR}, nil
	default:
		return r, errors.New("Invalid value for argument key")
	}
}

/*
	Returns the super region of a region
	type indicates whether current region is a ne, nw, se or sw quadrant
*/
func (r RegionBoundary) GetSuperregion(type_q string, level int8) (RegionBoundary, error) {

	const worldLatH = 90
	const worldLatL = -90
	const worldLongH = 180
	const worldLongL = -180

	if type_q != "ne" && type_q != "nw" && type_q != "se" && type_q != "sw" {
		return r, errors.New("Invalid value for argument type")
	}

	rc := r

	var i int8
	for i = 0; i < level; i++ {

		var newUL, newLR Point

		if type_q == "se" || type_q == "sw" {
			newUL.Lat = rc.UpperLeft.Lat + math.Abs(rc.UpperLeft.Lat-rc.LowerRight.Lat)
			newLR.Lat = rc.LowerRight.Lat
		}
		if type_q == "ne" || type_q == "nw" {
			newUL.Lat = rc.UpperLeft.Lat
			newLR.Lat = rc.LowerRight.Lat - math.Abs(rc.UpperLeft.Lat-rc.LowerRight.Lat)
		}

		//simple cases where next level region won't cross +/- 180
		if type_q == "nw" || type_q == "sw" {
			newUL.Long = rc.UpperLeft.Long
			newLR.Long = rc.LowerRight.Long + math.Abs(rc.UpperLeft.Long-rc.LowerRight.Long)
		}
		if type_q == "ne" || type_q == "se" {
			newLR.Long = rc.LowerRight.Long
			newUL.Long = rc.UpperLeft.Long - math.Abs(rc.UpperLeft.Long-rc.LowerRight.Long)
		}

		//region boundary doesn't contain +/- 180 but the next level will
		if rc.UpperLeft.Long < rc.LowerRight.Long {
			width := math.Abs(rc.UpperLeft.Long - rc.LowerRight.Long)
			if math.Abs(rc.UpperLeft.Long-rc.LowerRight.Long) > math.Abs(rc.UpperLeft.Long-180) {
				if type_q == "ne" || type_q == "se" {
					diff := math.Abs(180 - rc.UpperLeft.Long)
					newUL.Long = (180 - math.Abs(width-diff))
				}
			}
			if math.Abs(rc.UpperLeft.Long-rc.LowerRight.Long) > math.Abs(-180-rc.LowerRight.Long) {
				if type_q == "nw" || type_q == "sw" {
					diff := math.Abs(180 - rc.LowerRight.Long)
					newLR.Long = -180 + math.Abs(width-diff)
				}
			}
		}

		//current region boundary crosses +/- 180
		if rc.UpperLeft.Long > rc.LowerRight.Long {
			width := math.Abs(180-rc.UpperLeft.Long) + math.Abs(-180-rc.LowerRight.Long)
			if type_q == "ne" || type_q == "se" {
				newUL.Long = rc.UpperLeft.Long - width
			} else if type_q == "nw" || type_q == "sw" {
				newLR.Long = rc.LowerRight.Long + width
			}
		}

		rc = RegionBoundary{UpperLeft: newUL, LowerRight: newLR}
		if rc.UpperLeft.Lat > worldLatH || rc.UpperLeft.Long < worldLongL || rc.LowerRight.Lat < worldLatL || rc.LowerRight.Long > worldLongH {
			return r, errors.New("Can't expand region; Going over world boundaries")
		}

	}

	return rc, nil
}
