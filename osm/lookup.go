package osm

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

const (
	shimPort = 8000
	osmPort  = 80
	osm      = "127.0.0.1"
)

func createLookupUrl(host string, port int, tileKey TileKey) string {
	x, y, z := TileKeyToXyz(tileKey)
	formatString := "http://%s:%d/hot/%d/%d/%d.png"
	return fmt.Sprintf(formatString, host, port, z, x, y)
}

func createLookupUrlNoPort(host string, tileKey TileKey) string {
	x, y, z := TileKeyToXyz(tileKey)
	formatString := "http://%s/hot/%d/%d/%d.png"
	return fmt.Sprintf(formatString, host, z, x, y)
}

func (node *NodeState) Lookup(tileKey TileKey, w http.ResponseWriter, worldRegion RegionBoundary, myHost string) ([]byte, error, int) {
	//fmt.Println("hello")
	if !worldRegion.isIn(tileKey.Point) {
		err := errors.New("The tile you are requesting is currently not available at the moment. Please try again in sometime")
		//http.Error(w, err.Error(), http.StatusBadRequest)
		return []byte{}, err, http.StatusBadRequest
	}

	memCacheBackdoor := CreateBackdoor()

	var url string

	onCache, err := memCacheBackdoor.ExistsOnShim(tileKey)

	if err != nil {
		log.Println(err.Error())
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		return []byte{}, err, http.StatusInternalServerError
	}
	if onCache {
		url = createLookupUrlNoPort(myHost, tileKey)
		log.Println("** cache_hit: ", url) //TODO: Add current url here
		tile, err := memCacheBackdoor.GetTileFromCache(tileKey)
		if err != nil {
			log.Println(err.Error())
			//http.Error(w, err.Error(), http.StatusInternalServerError)
			return []byte{}, err, http.StatusInternalServerError
		}
		return tile, nil, 200
	}

	if node.Region.isIn(tileKey.Point) {
		node.MyLoad++
		region, renders := childRenders(tileKey.Point, node)
		if renders && node.Children[region] != "" {
			url = createLookupUrl(node.Children[region], shimPort, tileKey)
			log.Println("** redirect: ", url)
			tile, err := getTile(url, w)
			if err != nil {
				log.Println(err.Error())
				//http.Error(w, err.Error(), http.StatusInternalServerError)
				return []byte{}, err, http.StatusInternalServerError
			}
			err = memCacheBackdoor.SetTileInCache(tileKey, tile)
			if err != nil {
				log.Println(err.Error())
				//http.Error(w, err.Error(), http.StatusInternalServerError)
				return []byte{}, err, http.StatusInternalServerError
			}
			return tile, nil, 200
		} else {
			url = createLookupUrl(osm, osmPort, tileKey)
			onOsmCache := memCacheBackdoor.onOsmCache(tileKey)
			if onOsmCache {
				log.Println("** osm_cache_hit: ", url)
			} else {
				log.Println("** osm_render: ", url)
			}

			tile, err := getTile(url, w)

			if err != nil {
				log.Println(err.Error())
				//http.Error(w, err.Error(), http.StatusInternalServerError)
				return []byte{}, err, http.StatusInternalServerError
			}
			return tile, nil, 200
		}

	} else {

		level := int8(1)
		index, err := checkAncestor(tileKey, node, level)

		if err == nil {
			url = createLookupUrl(node.Ancestors[index], shimPort, tileKey)
			log.Println("** redirect: ", url)
		} else {
			//call world if all else fails.
			url = createLookupUrl(node.Ancestors[0], shimPort, tileKey)
			log.Println("** redirect: ", url)
		}

		tile, getTileErr := getTile(url, w)
		if getTileErr != nil {
			log.Println(getTileErr.Error())
			//http.Error(w, getTileErr.Error(), http.StatusInternalServerError)
			return []byte{}, getTileErr, http.StatusInternalServerError
		}

		err = memCacheBackdoor.SetTileInCache(tileKey, tile)
		if err != nil {
			log.Println(err.Error())
			//http.Error(w, err.Error(), http.StatusInternalServerError)
			return []byte{}, err, http.StatusInternalServerError
		}

		return tile, nil, 200
	}
	log.Println("end of lookup")
	return []byte{}, nil, 500
}

func childRenders(p Point, node *NodeState) (string, bool) {
	regions := []string{"ne", "nw", "se", "sw"}
	for _, r := range regions {
		rb, err := node.Region.GetSubregion(r)
		if err != nil {
			return "", false
		}
		if rb.isIn(p) {
			return r, true
		}
	}
	return "", false
}

func checkAncestor(tileKey TileKey, node *NodeState, level int8) (int, error) {
	if int(level) >= len(node.Ancestors) {
		return 0, errors.New("Couldn't find node in any of the region boundarys")
	}
	regions := []string{"ne", "nw", "se", "sw"}
	for _, r := range regions {
		rb, err := node.Region.GetSuperregion(r, level)
		if err != nil {
			return 0, err
		}
		if rb.isIn(tileKey.Point) {
			return int(level), nil
		}
	}
	level++
	checkAncestor(tileKey, node, level)
	return 0, nil
}

func getTile(url string, w http.ResponseWriter) ([]byte, error) {
	resp, respErr := http.Get(url)
	if respErr != nil {
		//http.Error(w, respErr.Error(), http.StatusInternalServerError)
		return []byte{}, respErr
	}
	if resp.StatusCode != 200 {
		//http.Error(w, "Apache error", http.StatusInternalServerError)
		return []byte{}, errors.New("Apache error bitch!")
	}
	body, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return body, nil

}
