package osm

import (
	"math"
)

func TileKeyToXyz(key TileKey) (int, int, int) {
	var x, y int
	x = int(math.Floor((key.Point.Long + 180.0) / 360.0 * (math.Exp2(float64(key.Zoom)))))
	y = int(math.Floor((1.0 - math.Log(math.Tan(key.Point.Lat*math.Pi/180.0)+1.0/math.Cos(key.Point.Lat*math.Pi/180.0))/math.Pi) / 2.0 * (math.Exp2(float64(key.Zoom)))))

	return x, y, int(key.Zoom)
}

func XyzToTileKey(x, y, z int) TileKey {
	n := math.Pi - 2.0*math.Pi*float64(y)/math.Exp2(float64(z))
	var lat, lng float64
	lat = 180.0 / math.Pi * math.Atan(0.5*(math.Exp(n)-math.Exp(-n)))
	lng = float64(x)/math.Exp2(float64(z))*360.0 - 180.0

	return TileKey{Point: Point{Lat: lat, Long: lng}, Zoom: int64(z), X: x, Y: y}
}
