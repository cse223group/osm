package osm

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

func (node *NodeState) InformParent() {

	resp, err := http.PostForm(node.Parent+"/newChild",
		url.Values{"rKey": {node.RKey}})
	if err != nil {
		node.OrphanKidsAndDie()
	}
	if resp.StatusCode != 200 {
		node.OrphanKidsAndDie()
	}
	defer resp.Body.Close()
}

func (node *NodeState) FreeNode(newNode NodeState, hostname string) (NodeState, bool) {

	if len(newNode.Ancestors) == 0 {
		newNode.Ancestors = append(newNode.Ancestors, node.Ancestors[0])
	}
	if len(node.Children) == 0 {
		children := make(map[string]string)
		node.Children = children
	}
	node.Children["nw"] = hostname
	newNode.Parent = newNode.Ancestors[len(newNode.Ancestors)-1]
	newNode.Region, _ = node.Region.GetSubregion("nw")
	return newNode, true

	if node.MyLoad > node.MySubtreeLoad && len(node.Children) < 4 {

		rKs := [4]string{"ne", "se", "nw", "sw"}
		for _, k := range rKs {
			_, ok := node.Children[k]
			if !ok {
				node.Children[k] = hostname
				newNode.Parent = newNode.Ancestors[len(newNode.Ancestors)-1]
				newNode.Region, _ = node.Region.GetSubregion(k)
				return newNode, true
			}
		}

	}

	loads := make(map[string]uint64)

	for _, v := range node.Children {
		if v != "" {
			r, _ := http.Get("http://" + v + ":8000" + "/getLoad")
			rContent, _ := ioutil.ReadAll(r.Body)
			defer r.Body.Close()
			rText := string(rContent)
			l, _ := strconv.ParseUint(rText, 10, 64)
			loads[v] = l
		}
	}
	var max_load uint64
	max_load = 0
	var next string

	for k, v := range loads {
		if max_load <= v {
			max_load = v
			next = k
		}

	}

	newNode.Ancestors = append(newNode.Ancestors, next)
	return newNode, false
}
