package osm

type WorldData struct {
	NodeList  map[string]RegionBoundary
	CacheData map[string]TileKey
}
