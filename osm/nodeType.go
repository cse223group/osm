package osm

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

type tempDeathState struct {
	LoadInfo   map[string]uint64 //map of dead child's children; gets invalidated as mitigation protocol terminates
	Valid      bool              //true if mitigation protocol is still running
	DeadManKey string
}

//stores the node state of each node
type NodeState struct {
	RKey           string            //ne, nw, se or sw?
	Renders        bool              `json:renders`
	Parent         string            `json:parent`    //hostname of parent
	Children       map[string]string `json:children`  //map[ne/se/sw/nw]hostname
	Ancestors      []string          `json:ancestors` //list of hostnames
	Region         RegionBoundary    `json:region`
	MyLoad         uint64            `json:myLoad`
	MySubtreeLoad  uint64            `json:mySubtreeLoad` //apparent load
	DeathStateInfo tempDeathState    `json:DeathStateInfo`
	World          string            `json:World`
}

type AddState struct {
	Node     NodeState
	Done     bool
	HostName string
}

//Updates the subtree load
func (node *NodeState) UpdateSubtreeLoad(req uint64) {

	//value at any time will be maximum of loads of children, or the leaf nodes in your subtree
	if req >= node.MySubtreeLoad {
		node.MySubtreeLoad += req
	}
}

//Ping the parent and send load. If it is dead, implement mitigation strategy
func (node *NodeState) PingParent() {

	if node.Parent == "" {
		return
	}

	var loadParam string
	if node.MySubtreeLoad > node.MyLoad {
		loadParam = strconv.FormatUint(node.MySubtreeLoad, 10)
	} else {
		loadParam = strconv.FormatUint(node.MyLoad, 10)
	}

	h, _ := os.Hostname()
	urlString := "http://" + node.Parent + ":8000" + "/updateLoad/" + h
	v := url.Values{}
	v.Set("load", loadParam)
	resp, err := http.PostForm(urlString, v)
	if err != nil {
		node.HandleParentDeath()
	}
	if resp.StatusCode != 200 {
		node.HandleParentDeath()
	}
	defer resp.Body.Close()
	responseText, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		log.Printf("Can't read response of ping parent")
	}
	responseStr := string(responseText)
	if strings.Contains(responseStr, "Not my child") {
		node.HandleOrphaned()
	}
}
