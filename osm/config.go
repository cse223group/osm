package osm

type globalConfig struct {
	USE_WORLD_NODE_DNS          bool
	USE_LOCAL_ORPHAN_MITIGATION bool
}

var Config globalConfig

func initGlobalConfig() {

	Config = globalConfig{
		USE_WORLD_NODE_DNS:          true,
		USE_LOCAL_ORPHAN_MITIGATION: false,
	}

}
