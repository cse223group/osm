package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"oms/osm"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

func NodeStateHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("Error reading body: %v", err)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}
	var response osm.NodeState
	err = json.Unmarshal(body, &response)
	if err != nil {
		fmt.Println("Error unmarshaling")
		http.Error(w, "Unmarshaling error", http.StatusInternalServerError)
		return
	}
	if response.Parent != "" {
		node.Parent = response.Parent
	}
	if len(response.Ancestors) > 0 {
		node.Ancestors = response.Ancestors
	}

	node.Region = response.Region
	if response.Children != nil {
		node.Children = response.Children
	}
}

func OsmHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var x, y, z int

	x, _ = strconv.Atoi(vars["x"])
	y, _ = strconv.Atoi(vars["y"])
	z, _ = strconv.Atoi(vars["z"])
	tileKey := osm.XyzToTileKey(x, y, z)
	tile, err, errorCode := osm.OsmCall(tileKey, w, r.Host)
	if err != nil {
		http.Error(w, err.Error(), errorCode)
		return
	}
	if len(tile) == 0 {
		http.Error(w, "Unable to locate the tile", http.StatusInternalServerError)
		return
	}
	_, err = w.Write(tile)
	if err != nil {
		http.Error(w, "Unable to write response", http.StatusInternalServerError)
		return
	}

}

func LookupHandler(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	var x, y, z int

	x, _ = strconv.Atoi(vars["x"])
	y, _ = strconv.Atoi(vars["y"])
	z, _ = strconv.Atoi(vars["z"])
	tileKey := osm.XyzToTileKey(x, y, z)

	tile, err, errorCode := node.Lookup(tileKey, w, worldRegion, r.Host)
	if err != nil {
		http.Error(w, err.Error(), errorCode)
		return
	}
	if len(tile) == 0 {
		http.Error(w, "Unable to locate the tile", http.StatusInternalServerError)
		return
	}
	_, err = w.Write(tile)
	if err != nil {
		http.Error(w, "Unable to write response", http.StatusInternalServerError)
		return
	}
}

func LoadHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	hostname := vars["hostname"]
	//hostname := strings.Split(r.Host, ":")[0]
	//log.Println(r.Host)
	flag := false
	for _, value := range node.Children {
		if strings.Contains(value, hostname) {
			flag = true
		}
	}
	if !flag {
		http.Error(w, "You aren't my child!", http.StatusBadRequest)
		return
	}

	r.ParseForm()
	load, e := strconv.ParseUint(r.Form.Get("load"), 10, 64)
	if e != nil {
		log.Println(e.Error())
		http.Error(w, "parsing error", http.StatusInternalServerError)
		return
	}
	node.UpdateSubtreeLoad(load)
}

func DeadChildHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	deadNode := r.Form.Get("dead")

	if osm.Config.USE_LOCAL_ORPHAN_MITIGATION == true {

		found := false
		for _, child := range node.Children {
			if deadNode == child {
				found = true
				node.DeathStateInfo.Valid = true
				go func() {
					node.HandleChildDeath()
				}()
			}
		}
		if !found && !node.DeathStateInfo.Valid {
			http.Error(w, "Parent death mitigated; Orphan yourself", http.StatusInternalServerError)
			return
		}
	}

	load, e := strconv.ParseUint(r.Form.Get("myLoad"), 10, 64)
	if e != nil {
		http.Error(w, "Cannot parse load", http.StatusInternalServerError)
		return

	}
	node.RegisterChildDeath(r.RemoteAddr, deadNode, load)

}

func OrphanHandler(w http.ResponseWriter, r *http.Request) {
	node.OrphanKidsAndDie()
	newNode := &osm.NodeState{}
	response, err := json.Marshal(newNode)
	if err != nil {
		http.Error(w, "Marshaling error", http.StatusInternalServerError)
		return
	}

	_, err = w.Write(response)
	if err != nil {
		http.Error(w, "Unable to write response", http.StatusInternalServerError)
		return
	}

}

func UpgradeHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	node.Ancestors = node.Ancestors[:len(node.Ancestors)-1]
	node.Parent = r.RemoteAddr
	node.DeathStateInfo.DeadManKey = node.RKey
	node.Region, _ = node.Region.GetSuperregion(node.RKey, 1)
	node.RKey = r.Form.Get("rKey")
	for k, _ := range node.Children {
		delete(node.Children, k)
	}
	node.MySubtreeLoad = 0
	go func() {
		node.HandleChildDeath()
		node.GatherStateInfoForUpgrade()
	}()
	node.InformParent()
}

func ParentUpdateHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	new_parent := r.Form.Get("newParent")
	node.Parent = new_parent
	node.InformParent()
}

func HandleGetLoad(w http.ResponseWriter, r *http.Request) {

	load := ""
	if len(node.Children) == 0 {
		load = strconv.FormatUint(node.MyLoad, 10)
	} else {
		load = strconv.FormatUint(node.MySubtreeLoad, 10)
	}
	log.Println(load)
	w.Write([]byte(load))
	return

}

func HandleNewChild(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	rKey := r.Form.Get("rKey")
	node.Children[rKey] = r.RemoteAddr
	return

}

func HandleFreeNode(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("Error reading body: %v", err)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}
	var addState osm.AddState
	err = json.Unmarshal(body, &addState)
	if err != nil {
		fmt.Println("Error unmarshaling")
		http.Error(w, "Unmarshaling error", http.StatusInternalServerError)
		return
	}

	newNode, done := node.FreeNode(addState.Node, addState.HostName)
	state := osm.AddState{Node: newNode, Done: done, HostName: addState.HostName}
	bytes, _ := json.Marshal(state)
	resp := string(bytes)
	w.Write([]byte(resp))
	return
}
