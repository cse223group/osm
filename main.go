package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"oms/osm"
	"os"
	"time"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

var node osm.NodeState
var worldRegion osm.RegionBoundary
var memCacheBackdoor osm.Backdoor

func loadConfig() {
	file, e := ioutil.ReadFile("./config.json")
	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}
	json.Unmarshal(file, &node)
}

func loadWorldRegion() {
	file, e := ioutil.ReadFile("./worldRegion.json")
	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}
	json.Unmarshal(file, &worldRegion)
}

func propogateState() {

	for {
		time.Sleep(1 * time.Second)
		node.PingParent()
	}
}

func main() {
	fmt.Println("Loading initial config")
	loadConfig()
	loadWorldRegion()
	fmt.Println("Starting server")
	mux := mux.NewRouter()
	mux.HandleFunc("/hot/{z}/{x}/{y}.png", LookupHandler).Methods("GET")
	mux.HandleFunc("/cool/{z}/{x}/{y}.png", OsmHandler).Methods("GET")
	mux.HandleFunc("/updateLoad/{hostname}", LoadHandler).Methods("POST")
	mux.HandleFunc("/updateNodeState", NodeStateHandler).Methods("POST")
	mux.HandleFunc("/deadChild", DeadChildHandler).Methods("POST")
	mux.HandleFunc("/orphaned", OrphanHandler).Methods("GET")
	mux.HandleFunc("/upgrade", UpgradeHandler).Methods("GET")
	mux.HandleFunc("/getLoad", HandleGetLoad).Methods("GET")
	mux.HandleFunc("/freeNode", HandleFreeNode).Methods("POST")
	mux.HandleFunc("/parentUpdate", ParentUpdateHandler).Methods("POST")
	if node.Parent != "" {
		go func() {
			propogateState()
		}()
	}
	n := negroni.Classic()
	n.UseHandler(mux)
	n.Run(":8000")
}
