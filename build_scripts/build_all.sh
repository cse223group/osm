#!/bin/sh
set -e

echo "************"
echo "./autogen.sh"
echo "************"
./autogen.sh

echo "***********"
echo "./configure"
echo "***********"
./configure

echo "****"
echo "make"
echo "****"
make

echo "*****************"
echo "sudo make install"
echo "*****************"
sudo make install

echo "**************************"
echo "sudo make install-mod_tile"
echo "**************************"
sudo make install-mod_tile

echo "*************"
echo "sudo ldconfig"
echo "*************"
sudo ldconfig

echo "****************************"
echo "sudo service apache2 restart"
echo "****************************"
sudo service apache2 restart
