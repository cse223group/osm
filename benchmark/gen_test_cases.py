import numpy as np
import itertools as it
import math
import json
import random

def lat_lng_to_xyz(zoom, lat, lng):
  lat_rad = math.radians(lat)
  n = 2.0 ** zoom
  x = int((lng + 180.0) / 360.0 * n)
  y = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
  return (zoom, x, y)

def create_url(i, case):
    return "http://tileserver{}.cloudapp.net:{{}}".format(i % 5 + 1) + \
          "/hot/{}/{}/{}.png".format(*case)

lats  = np.arange(37.1, 43.0, 0.1)
lngs  = np.arange(45.1, 51.0, 0.1)
zooms = np.arange(10, 18, 1)

latlng_prod  = it.product(zooms, lats, lngs)

xyz_prod = [lat_lng_to_xyz(*i) for i in latlng_prod]
random.shuffle(xyz_prod)
xyz_prod = [create_url(*tup) for tup in enumerate(xyz_prod)]

print len(xyz_prod)
json.dump(xyz_prod, open('test_cases.json', 'w'), indent=4)