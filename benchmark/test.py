import json
from multiprocessing import Pool
import requests
from functools import partial

PORT = 8000
NUM_WORKERS = 10
NUM_REQUESTS = 10

def timedelta_to_ms(delta):
    return (delta.days * 86400000) + (delta.seconds * 1000) + (delta.microseconds / 1000)

def send_req(case):
    url = case.format(PORT)
    print url
    try:
        res = requests.get(url)
        if res.status_code == 200 and res.headers['content-type'] == 'image/png':
            return timedelta_to_ms(res.elapsed)
        else: return None

    except Exception as e:
        print e
        return False

def main():
    pool = Pool(processes=NUM_WORKERS)
    all_test_cases = json.load(open('cases.json'))
    test_cases = all_test_cases[:NUM_REQUESTS]
    times = pool.map(send_req, test_cases)

    print times


if __name__ == '__main__':
    main()